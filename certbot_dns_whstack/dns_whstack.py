"""DNS Authenticator for WhStack."""

import logging

import zope.interface
from certbot import interfaces
from certbot.plugins import dns_common

logger = logging.getLogger(__name__)


@zope.interface.implementer(interfaces.IAuthenticator)
@zope.interface.provider(interfaces.IPluginFactory)
class Authenticator(dns_common.DNSAuthenticator):
    """DNS Authenticator for WhStack DNS."""

    description = (
        "Obtain certificates using a DNS TXT record (if you are using WhStack for DNS.)"
    )

    ttl = 60

    def __init__(self, *args, **kwargs):
        super(Authenticator, self).__init__(*args, **kwargs)
        self.credentials = None

    @classmethod
    def add_parser_arguments(cls, add):
        super(Authenticator, cls).add_parser_arguments(
            add, default_propagation_seconds=60
        )
        add("credentials", help="WhStack credentials file.")

    def more_info(self):  # pylint: disable=missing-docstring,no-self-use
        return "This plugin configures a DNS TXT record to respond to a dns-01 challenge using WhStack API"

    def _setup_credentials(self):
        self._configure_file("credentials", "Absolute path to WhStack credentials file")
        dns_common.validate_file_permissions(self.conf("credentials"))
        self.credentials = self._configure_credentials(
            "credentials",
            "WhStack credentials file",
            {
                "api-url": "WhStack-compatible API FQDN",
                "api-user": "WhStack-compatible API user",
                "api-key": "WhStack-compatible API key",
            },
        )

    def _perform(self, domain, validation_name, validation):
        self._get_whstack_client().add_txt_record(domain, validation_name, validation)

    def _cleanup(self, domain, validation_name, validation):
        self._get_whstack_client().del_txt_record(domain, validation_name, validation)
